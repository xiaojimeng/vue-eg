import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
import axios from "axios";
import api from '@/assets/config/api.js'

export default new Vuex.Store({
  state: {
    cartTotal:
    {
      checkedGoodsAmount: 0,
      checkedGoodsCount: 0,
      goodsAmount: 0,
      goodsCount: 0
    },
    cartList: []
  },
  mutations: {
    // setCarList:function(state,cartList){
    //   state.cartList = cartList
    // },
    setCartList(state, cartList) {
      state.cartList = cartList
    },
    setCartTotal(state, cartTotal) {
      state.cartTotal = cartTotal
    }
  },
  actions: {
    // 获取购物车数据
    async AjaxCart(content) {
      let res = await axios.get(api.CartList);
      let {data}=res.data
      content.commit('setCartList', data.cartList)
      content.commit('setCartTotal', data.cartTotal)
      // console.log(data);
    }
  },
  modules: {
  }
})
