import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vant from 'vant';
import 'vant/lib/index.css';
import "./assets/css/reset.css";
// import './assets/css/style.less';
import api from './assets/config/api.js'
Vue.use(Vant);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
  data: { api }//this.$root.api
}).$mount('#app')
