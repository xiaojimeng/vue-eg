import Vue from 'vue'
import VueRouter from 'vue-router'

const Main = () => import('../views/main/Main')
const Home = () => import('../views/main/Home')
const Cart = () => import('../views/main/Cart')
const Project = () => import('../views/main/Project')
const Classify = () => import('../views/main/Classify')
const Mine = () => import('../views/main/Mine')
const ClassifyDet = () => import('../views/ClassifyDet')
const GoodsDet = () => import('../views/GoodsDet')




Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Main,
    redirect: '/home',
    children: [
      {
        path: '/home',
        name: 'home',
        component: Home
      },
      {
        path: '/cart',
        name: 'cart',
        component: Cart
      },
      {
        path: '/project',
        name: 'project',
        component: Project
      }, {
        path: '/classify',
        name: 'classify',
        component: Classify
      }, {
        path: '/mine',
        name: 'mine',
        component: Mine
      },
    ]
  },
  {
    path: '/classifydet',
    // path: '/classifydet/:id', :to="'/classify?id='v.id"
    name: 'classifydet',
    component: ClassifyDet
  },
  {
    path: '/goodsdet',
    name: 'goodsdet',
    component: GoodsDet
  },
  

]

const router = new VueRouter({
  routes,
  mode: 'history'
})

export default router
