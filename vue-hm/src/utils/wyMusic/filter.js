import Vue from 'vue'

//定义全局滤器
Vue.filter('playTimeFormat', function (duration) {
  const m = parseInt(duration / 1000 / 60);
  // const s = parseInt((duration / 1000) % 60);
  // if (s < 10) s = "0" + s;
  const s = (parseInt((duration / 1000) % 60) + "").padStart(2, 0);
  return `${m}:${s}`;
})
Vue.filter('playCountFormat', function (n) {
  if (n > 100000) {
    n = parseInt(n / 10000) + "万";
    return n;
  }
  return n;
})
Vue.filter('dateFormat', function (time) {
  time = new Date(time)
  const yy = time.getFullYear()
  // const mm = time.getMonth() + 1
  // if (mm < 10) mm = "0" + mm
  const mm = (time.getMonth() + 1 + '').padStart(2, 0)
  const dd = (time.getDate() + '').padStart(2, 0)
  const h = (time.getHours() + '').padStart(2, 0)
  const m = (time.getMinutes() + '').padStart(2, 0)
  const s = (time.getSeconds() + '').padStart(2, 0)

  return `${yy}-${mm}-${dd} ${h}:${m}:${s}`
})