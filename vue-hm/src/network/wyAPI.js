import axios from 'axios'
// 在请求拦截器 展示数据请求进度条，NProgress.start();
// 在响应拦截器 隐藏进度条 NProgress.done();
// import NProgress from 'nprogress'
// import 'nprogress/nprogress.css'


// 响应时间
// axios.defaults.timeout = 10 * 1000
// 配置cookie
// axios.defaults.withCredentials = true

// 配置请求头
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'

// 静态资源
// Vue.prototype.$static = ''

// 配置接口地址
axios.defaults.baseURL = 'https://autumnfish.cn'

// 请求拦截
axios.interceptors.request.use(config => {
  // NProgress.start()
  return config
})

// 响应拦截
axios.interceptors.response.use(res => {
  // NProgress.done()
  return res
})
export default {
 get(url, params) {
    return axios.get(url, params)//get请求，请求参数必须是{params:xxx}
  }
}
