import Vue from 'vue'
import {
  Button, 
  Select, 
  Input, 
  Icon, 
  Dialog, 
  Tag, 
  Carousel,
  CarouselItem,
  Pagination,
  Tabs,
  TabPane,
  Message,
  MessageBox,
} from 'element-ui'
Vue.use(Button)
Vue.use(Select)
Vue.use(Input)
Vue.use(Icon)
Vue.use(Dialog)
Vue.use(Tag)

Vue.use(Carousel)
Vue.use(CarouselItem)
Vue.use(Pagination)
Vue.use(Tabs)
Vue.use(TabPane)

Vue.prototype.$message = Message
Vue.prototype.$confirm = MessageBox.confirm



