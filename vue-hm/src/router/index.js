import Vue from 'vue'
import VueRouter from 'vue-router'

const Home = () => import('../view/Home')
const Book = () => import('../view/Book')
const Axios = () => import('../view/Axios')
const Music = () => import('../view/Music')
const Wmusic = () => import('../view/wyMusic/Wmusic')
const Discovery = () => import('../view/wyMusic/Discovery')
const Playlists = () => import('../view/wyMusic/Playlists')
const Songs = () => import('../view/wyMusic/Songs')
const Mvs = () => import('../view/wyMusic/Mvs')
const Result = () => import('../view/wyMusic/Result')
const MvDetail = () => import('../view/wyMusic/MvDetail')
const ListDetail = () => import('../view/wyMusic/ListDetail')

Vue.use(VueRouter)
const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    component: Home
  },

  {
    path: '/book',
    component: Book
  },
  {
    path: '/axios',
    component: Axios
  },

  {

    path: '/music',
    component: Music

  },
  {

    path: '/wmusic',
    component: Wmusic,
    children: [
      {
        path:'/',
        redirect: '/wmusic/discovery'
      },
      {
        path: 'discovery',
        component: Discovery
      },
      {
        path: 'playlists',
        component: Playlists
      },
      {
        path: 'songs',
        component: Songs
      },
      {
        path: 'mvs',
        component: Mvs
      },
      {
        path: 'result',
        name:'result',
        component: Result
      },
      {
        path: 'mvdetail',
        name:'mvdetail',
        component: MvDetail
      },
      {
        path: 'listdetail',
        name:'listdetail',
        component: ListDetail
      }
    ]
  }
]
const router = new VueRouter({
  routes,
  mode: "history"
})
export default router
