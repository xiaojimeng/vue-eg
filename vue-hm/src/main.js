import Vue from 'vue'
import router from './router/index.js'
// import 'element-ui/lib/theme-chalk/index.css'//element的css样式
import './element.js'
import App from './App.vue'
import axios from 'axios'
import './assets/font/iconfont.css'//字体图标

import './assets/wymusic.css'
import './utils/wyMusic/filter.js'
import wyAPI from './network/wyAPI.js'

Vue.config.productionTip = false
Vue.prototype.$api = axios//使用：axios=this.$api
Vue.prototype.$wyAPI=wyAPI


new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
